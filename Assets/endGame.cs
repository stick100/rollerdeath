﻿using UnityEngine;
using System.Collections;

public class endGame : MonoBehaviour {
	
	public GameObject winzone;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void FixedUpdate()
	{
		float distance = Vector3.Distance (this.transform.position, winzone.transform.position);
		if(distance < 3)
		{
			Time.timeScale = 0.0F;
		}
	}
}
