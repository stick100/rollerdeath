﻿using UnityEngine;
using System.Collections;

public class rollto : MonoBehaviour {
	
	public Transform goal;
	public Collider winCondition;
    public Rigidbody body;
    public float speed = 10;
    public float maxSpeed = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void FixedUpdate()
    {
        this.transform.LookAt(goal);
        if(body.velocity.magnitude < maxSpeed)
	        body.AddForce(this.transform.forward*speed, ForceMode.Acceleration);
	    if(winCondition.GetComponent<Collider>().bounds.Contains(this.transform.localPosition))
	    {
	    	Time.timeScale = 0;//End
	    }
    }
}
