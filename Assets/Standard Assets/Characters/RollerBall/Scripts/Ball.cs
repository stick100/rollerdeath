﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Vehicles.Ball
{
    public class Ball : MonoBehaviour
    {
	    [SerializeField] public float m_MovePower_Setting = 5; // The force added to the ball to move it.
        [SerializeField] private bool m_UseTorque = true; // Whether or not to use torque to move the ball.
        [SerializeField] private float m_MaxAngularVelocity = 25; // The maximum velocity the ball can rotate at.
        [SerializeField] private float m_JumpPower = 2; // The force added to the ball when it jumps.

        public Text uiText;

        private const float k_GroundRayLength = 1f; // The length of the ray to check if the ball is grounded.
        private Rigidbody m_Rigidbody;

	    private float m_MovePower;
	    public Rigidbody body;
	    public SphereCollider collider;
	    private float initialMass;
	    private float initialSize;
	    private float initialDrag;

        private void Start()
	    {
		    initialMass = body.mass;
		    initialSize = this.transform.localScale.x;
		    initialDrag = body.angularDrag;
            m_MovePower = m_MovePower_Setting;
            m_Rigidbody = GetComponent<Rigidbody>();
            // Set the maximum angular velocity.
            GetComponent<Rigidbody>().maxAngularVelocity = m_MaxAngularVelocity;
        }

        private void FixedUpdate()
        {
	        //m_MovePower += .1f;
	        if (Input.GetMouseButton(0)){
			    m_MovePower = m_MovePower_Setting*3;
	        	body.transform.localScale.Set(initialSize*3,initialSize*3,initialSize*3);
	        }
	        else
	        {
		        m_MovePower = m_MovePower_Setting;
	        }
	        
	        if (Input.GetMouseButton(1))
	        {
		        body.drag = initialDrag*5;
	        }
	        else
	        {
		        body.drag = initialDrag;
	        }
	        
	        uiText.text = body.transform.localScale.x.ToString() + m_MovePower + body.drag;
        }

        public void Move(Vector3 moveDirection, bool jump)
        {
            // If using torque to rotate the ball...
            if (m_UseTorque)
            {
                // ... add torque around the axis defined by the move direction.
                m_Rigidbody.AddTorque(new Vector3(moveDirection.z, 0, -moveDirection.x)*m_MovePower);
            }
            else
            {
                // Otherwise add force in the move direction.
                m_Rigidbody.AddForce(moveDirection*m_MovePower);
            }

            // If on the ground and jump is pressed...
            if (Physics.Raycast(transform.position, -Vector3.up, k_GroundRayLength) && jump)
            {
                // ... add force in upwards.
                m_Rigidbody.AddForce(Vector3.up*m_JumpPower, ForceMode.Impulse);
            }
        }
    }
}
