﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Object = UnityEngine.Object;

public class killBelow : MonoBehaviour {
	
	public float deathY = -10;
    public Text scoreText;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (this.transform.localPosition.y < deathY)
	    {
	        int score = Int32.Parse(scoreText.text);
		    scoreText.text = (score + 1).ToString();
            Object.Destroy(this.gameObject);
        }
	}
}
