﻿using UnityEngine;
using System.Collections;

public class spawnEnemy : MonoBehaviour {
	
	public GameObject toSpawn;
	public float rate = 5;
	public float radius;
	private float timeSinceLastSpawn = 0;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceLastSpawn += Time.deltaTime;

	    if (timeSinceLastSpawn > rate)
	    {
	        GameObject.Instantiate(toSpawn, new Vector3(this.transform.localPosition.x + Random.Range(-1*radius, radius)
	            , this.transform.localPosition.y + Random.Range(-1*radius, radius)
	            , this.transform.localPosition.z + Random.Range(-1*radius, radius)), new Quaternion());
	        timeSinceLastSpawn = 0;
	    }
	}
}
