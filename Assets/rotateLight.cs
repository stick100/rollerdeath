﻿using UnityEngine;
using System.Collections;

public class rotateLight : MonoBehaviour
{
    public float speedToRot = .1f; 

    public Light toRotatate;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
	    toRotatate.transform.Rotate(Vector3.up, speedToRot);
    }
}
